package com.example.demo.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Factura;
import com.example.demo.interfaces.FacturaInterface;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/factura")
public class FacturaService {

	@Autowired
	public FacturaInterface Factura;

	@GetMapping
	public ResponseEntity<List<Factura>> getFacturas() {

		List<Factura> facturas = Factura.findAll();

		return ResponseEntity.ok(facturas);
	}

	@RequestMapping(value = "/{facturaId}")
	public ResponseEntity<Factura> getFacturaById(@PathVariable("facturaId") Integer facturaId) {
		Optional<Factura> optionalFactura = Factura.findById(facturaId);
		if (optionalFactura.isPresent()) {
			return ResponseEntity.ok(optionalFactura.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Factura> saveFactura(@RequestBody Factura fact) {
		Factura facturaNueva = Factura.save(fact);
		return ResponseEntity.ok(facturaNueva);
	}
}
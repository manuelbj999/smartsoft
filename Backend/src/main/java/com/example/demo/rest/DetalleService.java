package com.example.demo.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Detalle;
import com.example.demo.interfaces.DetalleInterface;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/detalle")
public class DetalleService {

	@Autowired
	public DetalleInterface Detalle;

	@GetMapping
	public ResponseEntity<List<Detalle>> getDetalles() {

		List<Detalle> detalles = Detalle.findAll();

		return ResponseEntity.ok(detalles);
	}

	@RequestMapping(value = "/{detalleId}")
	public ResponseEntity<Detalle> getDetalleById(@PathVariable("detalleId") Integer detalleId) {
		Optional<Detalle> optionalDetalle = Detalle.findById(detalleId);
		if (optionalDetalle.isPresent()) {
			return ResponseEntity.ok(optionalDetalle.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Detalle> saveDetalle(@RequestBody Detalle dt) {
		Detalle detalleNuevo = Detalle.save(dt);
		return ResponseEntity.ok(detalleNuevo);
	}
}
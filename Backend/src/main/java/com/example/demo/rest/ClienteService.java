package com.example.demo.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Cliente;
import com.example.demo.interfaces.ClienteInterface;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/cliente")
public class ClienteService {

	@Autowired
	public ClienteInterface cliente;
	
	
	@GetMapping
	public ResponseEntity<List<Cliente>> getClientes(){
		
		List<Cliente> clientes = cliente.findAll();
		
		return ResponseEntity.ok(clientes);
	}
	
	@RequestMapping(value="/{clienteId}")
	public ResponseEntity<Cliente> getClienteById(@PathVariable("clienteId") Integer clienteId){
		Optional<Cliente> optionalCliente= cliente.findById(clienteId);
		if(optionalCliente.isPresent()) {
			return ResponseEntity.ok(optionalCliente.get());
		}
		else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cl){
	Cliente clienteNuevo= cliente.save(cl);
	return ResponseEntity.ok(clienteNuevo);
	}
}

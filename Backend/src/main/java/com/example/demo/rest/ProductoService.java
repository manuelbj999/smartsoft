package com.example.demo.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Producto;
import com.example.demo.interfaces.ProductoInterface;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/producto")
public class ProductoService {

	@Autowired
	public ProductoInterface Producto;

	@GetMapping
	public ResponseEntity<List<Producto>> getProductos() {

		List<Producto> productos = Producto.findAll();

		return ResponseEntity.ok(productos);
	}

	@RequestMapping(value = "/{productoId}")
	public ResponseEntity<Producto> getProductoById(@PathVariable("productoId") Integer productoId) {
		Optional<Producto> optionalProducto = Producto.findById(productoId);
		if (optionalProducto.isPresent()) {
			return ResponseEntity.ok(optionalProducto.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Producto> saveProducto(@RequestBody Producto prod) {
		Producto productoNuevo = Producto.save(prod);
		return ResponseEntity.ok(productoNuevo);
	}
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.entities;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Manuel
 */
@Entity
@Table(name = "detalle")
public class Detalle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "num_detalle")
    private Integer numDetalle;
    @Basic(optional = false)
    @Column(name = "cantidad", nullable = false)
    private int cantidad;
    @Basic(optional = false)
    @Column(name = "precio", nullable = false)
    private int precio;
    @JoinColumn(name = "num_factura", referencedColumnName = "num_factura")
    @ManyToOne(optional = false)
    private Factura numFactura;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private Producto idProducto;

    public Detalle() {
    }

    public Detalle(Integer numDetalle) {
        this.numDetalle = numDetalle;
    }

    public Detalle(Integer numDetalle, int cantidad, int precio) {
        this.numDetalle = numDetalle;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public Integer getNumDetalle() {
        return numDetalle;
    }

    public void setNumDetalle(Integer numDetalle) {
        this.numDetalle = numDetalle;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Factura getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(Factura numFactura) {
        this.numFactura = numFactura;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numDetalle != null ? numDetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalle)) {
            return false;
        }
        Detalle other = (Detalle) object;
        if ((this.numDetalle == null && other.numDetalle != null) || (this.numDetalle != null && !this.numDetalle.equals(other.numDetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "api.Detalle[ numDetalle=" + numDetalle + " ]";
    }
    
}

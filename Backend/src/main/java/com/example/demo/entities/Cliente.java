package com.example.demo.entities;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cliente")
public class Cliente {

	// private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id_cliente")
	private Integer idCliente;

	@Basic(optional = false)
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;

	@Basic(optional = false)
	@Column(name = "apellido", nullable = false, length = 50)
	private String apellido;
	
	@Basic(optional = false)
	@Column(name = "direccion", nullable = false, length = 50)
	private String direccion;
	
	@Basic(optional = false)
	@Column(name = "fecha_nacimiento", nullable = false, length = 50)
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@Basic(optional = false)
	@Column(name = "telefono", nullable = false, length = 50)
	private int telefono;
	
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider using
	// this annotation to enforce field validation
	@Basic(optional = false)
	@Column(name = "email", nullable = false, length = 50)
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idCliente")
	private Collection<Factura> facturaCollection;

	public Cliente() {
    }

	public Cliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public void setFacturaCollection(Collection<Factura> facturaCollection) {
		this.facturaCollection = facturaCollection;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Cliente(Integer idCliente, String nombre, String apellido, String direccion, Date fechaNacimiento, int telefono, String email) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.fechaNacimiento = fechaNacimiento;
        this.telefono = telefono;
        this.email = email;
    }

	public Integer getIdCliente() {
		return idCliente;
	}

}

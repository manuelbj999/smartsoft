package com.example.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.entities.Producto;

public interface ProductoInterface extends JpaRepository<Producto, Integer>{

}

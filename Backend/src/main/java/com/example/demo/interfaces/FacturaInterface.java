package com.example.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.entities.Factura;

public interface FacturaInterface extends JpaRepository<Factura, Integer>{

}

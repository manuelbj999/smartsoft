package com.example.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Cliente;

public interface ClienteInterface extends JpaRepository<Cliente, Integer>{

}

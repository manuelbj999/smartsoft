import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  baseUrl = environment.baseUrl+"facturas";

  constructor(
    public http: HttpClient
  ) { }

  getFacturas(){
    return this.http.get(this.baseUrl);
  }

  saveFactura(facturas:any){
    return this.http.post(this.baseUrl,facturas);
  }
}

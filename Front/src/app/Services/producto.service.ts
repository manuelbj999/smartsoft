import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductoService {
  url=environment.baseUrl+"producto"
  constructor(public http: HttpClient) {}

  public getProducts(){
    return this.http.get(this.url);
  }
  public getProductById(id:number){
    return this.http.get(this.url+`/${id}`);
  }
}


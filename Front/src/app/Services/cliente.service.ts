import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Cliente } from '../Interfaces/cliente';

@Injectable({
  providedIn: 'root',
})
export class ClienteService {

  baseUrl=environment.baseUrl+'cliente';
  constructor(public http: HttpClient) {}

  public getClientes() {
    return this.http.get(this.baseUrl);
  }

  public saveCliente(cliente:Cliente){
    return this.http.post(this.baseUrl, cliente)
  }
}

import { Component } from '@angular/core';
import { Producto } from './Interfaces/producto';
import { ClienteService } from './Services/cliente.service';
import { FacturaService } from './Services/factura.service';
import { ProductoService } from './Services/producto.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Front';

  clientes: any[] | undefined;
  fecha: Date | undefined;
  productos: any[] | undefined;
  nuevaFactura=null;


  constructor(
    public clientService: ClienteService,
    public productoService: ProductoService,
    public facturaService:FacturaService
  ) {
    this.fecha = new Date();

    this.getClientes();
    this.getProductos();
  }

  public getClientes() {
    this.clientService.getClientes().subscribe((data: any) => {
      this.clientes = data;
    });
  }

  public getProductos() {
    this.productoService.getProducts().subscribe((data:any)=>{
      this.productos=data;
    })
  }
  public saveFactura(factura:any){
    console.log(factura);
    // this.facturaService.saveFactura(factura).subscribe((data:any)=>{
    //   this.nuevaFactura=data;
    // });
  }
}

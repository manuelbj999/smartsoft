export interface Detalle {
  numDetalle:number;
  idFactura:number;
  idProducto:number;
  cantidad:number;
  precio:number;
}
